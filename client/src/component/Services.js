import React from "react";
import { BsFillHddStackFill, BsCodeSlash } from "react-icons/bs";
import "../style/Services.css";
const Services = () => {
  return (
    <div className="services-container" id="services">
      <div className="services-maxwidth">
        <div className="services-title">My Services</div>
        <div className="services-content">
          <div className="services-box">
            <div className="services-card" id="card-backend">
              <i>
                <BsFillHddStackFill />
              </i>
              <div className="services-intro">Backend Development</div>
              <p className="services-description">
                Develop your server/backend using node js include testing and
                using the most suitable database for you.
              </p>
            </div>

            <div className="services-card">
              <i>
                <BsCodeSlash />
              </i>
              <div className="services-intro">Frontend Development</div>
              <p className="services-description">
                Develop your client/frontend using react js include modern UI
                which make visitors enjoy to visit your website.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Services;
