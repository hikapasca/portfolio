import React from "react";
import "../style/Project.css";

import imageTraveli from "../asset/traveli.png";
import imagePokemon from "../asset/pokemon.png";
import imageMovie from "../asset/movie.png";
import { FiChevronRight } from "react-icons/fi";

import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Mousewheel,
  Keyboard,
} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
SwiperCore.use([Navigation, Pagination, Mousewheel, Keyboard]);
const Project = () => {
  return (
    <div className="project-container" id="project">
      <div className="project-maxwidth">
        <div className="project-intro">My Project</div>
        <div className="project-contentContainer">
          <div>
            <Swiper
              cssMode={true}
              navigation={true}
              pagination={true}
              mousewheel={true}
              keyboard={true}
              className="mySwiper"
            >
              <SwiperSlide>
                {" "}
                <div className="project-content">
                  <img
                    src={imagePokemon}
                    alt="image-pokemon"
                    className="project-img"
                  />
                  <div className="project-data">
                    <h3 className="project-title">Pokemon List</h3>
                    <p className="project-description">
                      A website that contains the list of pokemon. You can pick
                      and release pokemon on this website.
                    </p>
                    <a
                      className="project-button"
                      href="https://pokemon-6c98d.web.app/"
                      target="_blank"
                    >
                      Visit
                    </a>
                  </div>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="project-content">
                  <img
                    src={imageTraveli}
                    alt="image-traveli"
                    className="project-img"
                  />
                  <div className="project-data">
                    <h3 className="project-title">Traveli Compro</h3>
                    <p className="project-description">
                      A website that contains Traveli company profile. This
                      website also having a cms.
                    </p>
                    <a
                      className="project-button"
                      href="https://traveli.co.id/"
                      target="_blank"
                    >
                      Visit
                    </a>
                  </div>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="project-content">
                  <img
                    src={imageMovie}
                    alt="image-movie"
                    className="project-img"
                  />
                  <div className="project-data">
                    <h3 className="project-title">Movie List</h3>
                    <p className="project-description">
                      A website that contains the list of movie. You can add
                      your favorite movies.
                    </p>
                    <a
                      className="project-button"
                      href="https://movies-app-6d738.web.app/"
                      target="_blank"
                    >
                      Visit
                    </a>
                  </div>
                </div>
              </SwiperSlide>
            </Swiper>
            {/* <div className="project-content">
              <img
                src={imagePokemon}
                alt="image-pokemon"
                className="project-img"
              />
              <div className="project-data">
                <h3 className="project-title">Pokemon List</h3>
                <p className="project-description">
                  A website that contains the list of pokemon. You can pick and
                  release pokemon on this website.
                </p>
                <a
                  className="project-button"
                  href="https://pokemon-6c98d.web.app/"
                  target="_blank"
                >
                  Visit
                </a>
              </div>
            </div>
            <div className="project-content">
              <img
                src={imageTraveli}
                alt="image-traveli"
                className="project-img"
              />
              <div className="project-data">
                <h3 className="project-title">Traveli Compro</h3>
                <p className="project-description">
                  A website that contains Traveli company profile. This website
                  also having a content management system.
                </p>
                <a
                  className="project-button"
                  href="https://traveli.co.id/"
                  target="_blank"
                >
                  Visit
                </a>
              </div>
            </div>
            <div className="project-content">
              <img src={imageMovie} alt="image-movie" className="project-img" />
              <div className="project-data">
                <h3 className="project-title">Movie List</h3>
                <p className="project-description">
                  A website that contains the list of movie. You can add your
                  favorite movies.
                </p>
                <a
                  className="project-button"
                  href="https://movies-app-6d738.web.app/"
                  target="_blank"
                >
                  Visit
                </a>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Project;
