import React from "react";
import ContactImage from "../asset/background-home.png";
import "../style/Contact.css";
import { HiOutlineMail } from "react-icons/hi";
import { BsWhatsapp, BsLinkedin, BsGithub } from "react-icons/bs";
import { SiGitlab } from "react-icons/si";
import { MdLocationOn } from "react-icons/md";
const Contact = () => {
  return (
    <div className="contact-container" id="contact">
      <div className="contact-maxwidth">
        <div className="contact-intro">Contact Me</div>
        <div className="contact-content">
          <div className="contact-left">
            <div className="contact-leftTop">
              <div className="contact-data">
                <i className="contact-icon">
                  <BsWhatsapp />
                </i>
                <div className="contact-detail">
                  <h3>WhatsApp</h3>
                  <p>+6281225023666</p>
                </div>
              </div>
              <div className="contact-data">
                <i className="contact-icon">
                  <HiOutlineMail />
                </i>
                <div className="contact-detail">
                  <h3>Email</h3>
                  <p>hikapasca@gmail.com</p>
                </div>
              </div>
              <div className="contact-data">
                <i className="contact-icon">
                  <MdLocationOn />
                </i>
                <div className="contact-detail">
                  <h3>Location</h3>
                  <p>Karanganyar, Indonesia</p>
                </div>
              </div>
            </div>
            <div className="contact-leftMiddle">
              <h3>Visit My linkedin:</h3>
              <a
                className="contact-icon"
                target="_blank"
                href="https://www.linkedin.com/in/hikapasca/"
              >
                <BsLinkedin />
              </a>
            </div>
            <div className="contact-leftBottom">
              <h3>Check my Code's here:</h3>
              <a
                className="contact-icon"
                target="_blank"
                href="https://github.com/hikapasca/"
              >
                <BsGithub />
              </a>

              <a
                className="contact-icon"
                target="_blank"
                href="https://gitlab.com/hikapasca/"
              >
                <SiGitlab />
              </a>
            </div>
          </div>
          <div className="contact-right">
            <img
              className="contact-image"
              src={ContactImage}
              alt="contact-image"
            ></img>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
