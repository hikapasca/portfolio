import React, { useState, useEffect } from "react";
import "../style/Navbar.css";
import { VscThreeBars, VscClose } from "react-icons/vsc";

const Navbar = () => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const [classNavbar, setClassNavbar] = useState("");
  const [menuActive, setMenuActive] = useState("off");
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };
  const handleClassNavbar = () => {
    if (classNavbar.length === 0) {
      return "navbar-container";
    } else {
      return "navbar-container navbar-sticky";
    }
  };

  const changeNavbarMenuHandler = () => {
    menuActive === "off" ? setMenuActive("on") : setMenuActive("off");
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    if (scrollPosition > 20) {
      setClassNavbar("navbar-sticky");
    } else {
      setClassNavbar("");
    }
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });
  return (
    <nav className={handleClassNavbar()}>
      <div className="max-width">
        <div className="navbar-logo">
          <a href="#home">
            Portfo<span>lio.</span>
          </a>
        </div>
        <ul className="navbar-menu">
          <li>
            <a href="#home" className="navbar-menubtn">
              Home
            </a>
          </li>
          <li>
            <a href="#about" className="navbar-menubtn">
              About
            </a>
          </li>
          <li>
            <a href="#services" className="navbar-menubtn">
              Services
            </a>
          </li>
          <li>
            <a href="#skills" className="navbar-menubtn">
              Skills
            </a>
          </li>
          <li>
            <a href="#project" className="navbar-menubtn">
              Project
            </a>
          </li>
          <li>
            <a href="#contact" className="navbar-menubtn">
              Contact
            </a>
          </li>
        </ul>
        <div className={`navbar-menuIcon `} onClick={changeNavbarMenuHandler}>
          <i>{menuActive === "off" ? <VscThreeBars /> : <VscClose />}</i>
          <ul className={"navbar-listMenu" + " " + menuActive}>
            <li>
              <a href="#home" className="navbar-menubtn">
                Home
              </a>
            </li>
            <li>
              <a href="#about" className="navbar-menubtn">
                About
              </a>
            </li>
            <li>
              <a href="#services" className="navbar-menubtn">
                Services
              </a>
            </li>
            <li>
              <a href="#skills" className="navbar-menubtn">
                Skills
              </a>
            </li>
            <li>
              <a href="#project" className="navbar-menubtn">
                Project
              </a>
            </li>
            <li>
              <a href="#contact" className="navbar-menubtn">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
