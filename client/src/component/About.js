import React from "react";
import "../style/About.css";
import imgAbout from "../asset/profile-about.png";
import CVHika from "../asset/CV-MuhammadNurHikaPasca.pdf";
import { FiDownload } from "react-icons/fi";
const About = () => {
  return (
    <div className="about-container" id="about">
      <div className="about-maxwidth">
        <h2 className="about-title">About Me</h2>
        <div className="about-content">
          <div className="about-column about-left">
            <img src={imgAbout}></img>
          </div>
          <div className="about-column about-right">
            <div className="about-intro">
              I'm Hika and I'm a <span>Full Stack Engineer</span>
            </div>
            <p className="about-description">
              I graduated from Physics Universitas Gadjah Mada in 2019. In 2020,
              I had followed a Bootcamp IT in Hacktiv8 and graduated with a
              final score of more than 90. I have more than one year of
              experience as a Full Stack Engineer/Developer. The technology that
              I use for Backend development is Node js. For databases, I use
              NoSQL, MySQL, MongoDB, and Firestore. For Front End development, I
              use React js.
            </p>
            <a download="" href={CVHika}>
              Download CV
              <i>
                <FiDownload />
              </i>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
