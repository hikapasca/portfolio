import React from "react";
import "../style/Home.css";
import imageUrl from "../asset/background-home.png";

const Home = () => {
  return (
    <div className="home-container" id="home">
      <div className="home-maxwidth">
        <div className="home-image">
          <img className="images" src={imageUrl} alt="image-hika" />
        </div>
        <div className="home-content">
          <div className="home-intro">Hi, my name is</div>
          <div className="home-name">Muhammad Nur Hika Pasca</div>
          <div className="home-job">
            I'm a <span> Full Stack Engineer </span>
          </div>
          <a className="home-button" href="#contact">
            Contact Me
          </a>
        </div>
      </div>
    </div>
  );
};

export default Home;
