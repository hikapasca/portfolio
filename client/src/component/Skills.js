import React, { useState } from "react";
import "../style/Skills.css";
import { FiChevronDown } from "react-icons/fi";
import { BsFillHddStackFill, BsCodeSlash } from "react-icons/bs";

const Skills = () => {
  const [counter, setCounter] = useState(0);
  const [statusFrontEnd, setStatusFrontEnd] = useState(false);
  const [statusBackEnd, setStatusBackEnd] = useState(false);
  const backEndHandler = () => {
    if (!statusBackEnd) {
      setStatusBackEnd(true);
      setCounter(counter + 1);
    } else if (statusBackEnd) {
      setStatusBackEnd(false);
      setCounter(counter - 1);
    }
  };

  const frontEndHandler = () => {
    if (!statusFrontEnd) {
      setStatusFrontEnd(true);
      setCounter(counter + 1);
    } else if (statusFrontEnd) {
      setStatusFrontEnd(false);
      setCounter(counter - 1);
    }
  };
  return (
    <div
      className="skills-container"
      style={{ height: counter === 0 ? "100vh" : "auto" }}
      id="skills"
    >
      <div className="skills-maxwidth">
        <div className="skills-intro">Skills</div>
        <div className="skills-contentContainer">
          {/* skills 1 */}
          <div
            className={`${statusFrontEnd === true && "skills-contentFrontEnd"}`}
          >
            <div className="skills-header" onClick={frontEndHandler}>
              <i className="skills-icon">
                <BsCodeSlash />
              </i>
              <div>
                <h1 className="skills-title">Frontend Developer</h1>
                <span className="skills-subtitle">More than 1 year</span>
              </div>
              <i className="skills-arrow">
                <FiChevronDown />
              </i>
            </div>
            <div
              className={`skills-listFrontEnd ${
                statusFrontEnd === true && "skills-frontEndActive"
              }`}
            >
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">HTML</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-html"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">CSS</h3>
                  <span className="skills-number ">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-css"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">Javascript</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-javascript"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">React js</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-react"></span>
                </div>
              </div>
            </div>
          </div>
          <div
            className={`${statusFrontEnd === true && "skills-contentBackEnd"}`}
          >
            <div className="skills-header" onClick={backEndHandler}>
              <i className="skills-icon">
                <BsFillHddStackFill />
              </i>
              <div>
                <h1 className="skills-title">Backend Developer</h1>
                <span className="skills-subtitle">More than 1 year</span>
              </div>
              <i className="skills-arrow">
                <FiChevronDown />
              </i>
            </div>
            <div
              className={`skills-listBackEnd ${
                statusBackEnd === true && "skills-backEndActive"
              }`}
            >
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">Node js</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-nodejs"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">Express js</h3>
                  <span className="skills-number ">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-expressjs"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">Firebase</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-firebase"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">PostgreSQL</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-postgresql"></span>
                </div>
              </div>
              <div className="skills-data">
                <div className="skills-dataDetail">
                  <h3 className="skills-name">Mongodb</h3>
                  <span className="skills-number">80%</span>
                </div>
                <div className="skills-bar">
                  <span className="skills-percentage skills-mongodb"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skills;
