import React from "react";
import {
  About,
  Contact,
  Home,
  Navbar,
  Services,
  Skills,
  Project,
} from "../component/index";

const Portfolio = () => {
  return (
    <div>
      <Navbar />
      <Home />
      <About />
      <Services />
      <Skills />
      <Project />
      <Contact />
    </div>
  );
};

export default Portfolio;
